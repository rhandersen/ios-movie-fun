import Foundation
import UIKit

protocol CustomViewDelegate: class {
    func didClick(_ sender: UIButton)
}
