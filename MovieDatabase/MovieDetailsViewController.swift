import UIKit
import SnapKit

final class MovieDetailsViewController: UIViewController {
    let movie: Movie
    var movieManager: MovieManager = MovieManager()
    
    private(set) lazy var helpButton: UIButton = {
        var button = UIButton()
        button.setTitle("Help", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        return button
    }()
    
    private(set) lazy var actorsButton: UIButton = {
        var button = UIButton()
        button.setTitle("Actors", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        return button
    }()
    
    private(set) lazy var movieDescription: UILabel = {
        var label = UILabel()
        label.textColor = .black
        label.numberOfLines = 3
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    private(set) lazy var isFavoriteToggle: UISwitch = {
        var toggle = UISwitch()
        toggle.isOn = false
        return toggle
    }()
    
    private(set) lazy var movieImage: UIImageView = {
        var imageVIew = UIImageView()
        imageVIew.contentMode = .scaleAspectFit
        return imageVIew
    }()
    
    init(movie: Movie) {
        self.movie = movie
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addElementsToView()
        styleView()
        populateWithData()
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func styleView() {
        view.backgroundColor = .white
        
        self.movieDescription.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(10)
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
        
        self.helpButton.snp.makeConstraints { make in
            make.bottom.equalTo(movieImage.snp.top).offset(-10)
            make.leading.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
        
        self.actorsButton.snp.makeConstraints { make in
            make.bottom.equalTo(movieImage.snp.top).offset(-10)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
        
        self.isFavoriteToggle.snp.makeConstraints { make in
            make.bottom.equalTo(movieImage.snp.top).offset(-10)
            make.centerX.equalTo(view.safeAreaLayoutGuide)
        }
        
        self.movieImage.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.centerY).offset(10)
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
    }
    
    private func addElementsToView() {
        self.view.addSubview(self.movieDescription)
        self.view.addSubview(self.isFavoriteToggle)
        self.view.addSubview(self.movieImage)
        self.view.addSubview(self.helpButton)
        self.view.addSubview(self.actorsButton)
        
        self.isFavoriteToggle.addTarget(self, action: #selector(toggleFavorite(_:)), for: .valueChanged)
        self.helpButton.addTarget(self, action: #selector(showHelp(_:)), for: .touchUpInside)
        self.actorsButton.addTarget(self, action: #selector(showActors(_:)), for: .touchUpInside)
    }
    
    private func populateWithData() {
        setHeader(movie.title)
        movieDescription.text = movie.description
        isFavoriteToggle.isOn = FavoriteHelper.isFavorite(movie)
        setImage(movie)
    }
    
    func setImage(_ movie: Movie) {
        movieManager.getImageFor(movie: movie) { movieImage in
            guard let movieImage = movieImage else {
                return
            }
            DispatchQueue.main.async {
                self.movieImage.image = movieImage
            }
        }
    }
    
    func setHeader(_ title: String) {
        self.title = "\(title) details"
    }
    
    @objc func toggleFavorite(_ sender: Any) {
        var isFavorite = FavoriteHelper.isFavorite(movie)
        isFavorite.toggle()
        isFavoriteToggle.isOn = isFavorite
        FavoriteHelper.setFavorite(movie, isFavorite: isFavorite)
    }
    
    @objc func showHelp(_ sender: Any) {
        let helpViewController = HelpViewController(nibName: nil, bundle: nil)
        
        // Present shows a modal dialog
        self.present(helpViewController, animated: true)
    }
    
    @objc func showActors(_ sender: Any) {
        let actorsListViewController = ActorListViewController(movie: self.movie)
        
        // PushViewController navigates to view instead of showing a modal dialog
        self.navigationController?.pushViewController(actorsListViewController, animated: true)
    }
}
