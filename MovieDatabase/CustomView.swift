import UIKit

class CustomView: UIView {
    
    public weak var delegate: CustomViewDelegate?
    
    public var text1: String = "" {
        didSet {
            self.text1Label.text = text1
        }
    }
    public var text2: String = "" {
        didSet {
            self.text2Label.text = text2
        }
    }
    
    private(set) lazy var text1Label: UILabel = {
        var label = UILabel()
        return label
    }()
    
    private(set) lazy var text2Label: UILabel = {
        var label = UILabel()
        return label
    }()
    
    private(set) lazy var someButton: UIButton = {
        var button = UIButton()
        button.setTitle("Dismiss parent view from sub view", for: UIControl.State.normal)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return button
    }()
    
    private(set) lazy var stack: UIStackView = {
        var stack = UIStackView(arrangedSubviews: [text1Label, text2Label, someButton])
        stack.alignment = .center
        stack.axis = .vertical
        stack.spacing = 10
        stack.distribution = .fillEqually
        return stack
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(stack)
        
        self.someButton.addTarget(self, action: #selector(self.didClickButton(_:)), for: .touchUpInside)
        
        stack.snp.makeConstraints { make in
            make.edges.equalTo(self.safeAreaLayoutGuide).inset(10)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didReceiveMemoryWarning() {
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didClickButton(_ sender: UIButton) {
        delegate?.didClick(someButton)
    }

}
