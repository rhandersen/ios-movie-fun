import Foundation
import UIKit

class MovieManager {
    var moviesTask: URLSessionDataTask?
    var moviesImageTask: URLSessionDataTask?
    
    let movieSource = "https://yink.dk/ios-course-movies"

    func fetchMovies(completionHandler: @escaping ([Movie]?) -> Void) -> URLSessionDataTask {
        let url = "\(movieSource)/movies"
        moviesTask?.cancel()
        moviesTask = DataHelper.getDataDeserialized(from: url, completionHandler: completionHandler)
        return moviesTask!
    }
    
    func getImageFor(movie: Movie, completionHandler: @escaping (UIImage?) -> Void) -> URLSessionDataTask {

        let url = "\(movieSource)\(movie.imageUrl)"
        self.moviesImageTask?.cancel()
        
        moviesImageTask = DataHelper.getRawData(from: url) { data, _, error in
            guard let data = data, error == nil else {
                completionHandler(nil)
                return
            }
            completionHandler(UIImage(data: data))
            return
        }
        return moviesImageTask!
    }
    
}
