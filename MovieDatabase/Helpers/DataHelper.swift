//
//  DataHelper.swift
//  MovieDatabase
//
//  Created by Bruger on 28/09/2018.
//  Copyright © 2018 Trifork A/S. All rights reserved.
//

import Foundation

class DataHelper {
    static func getRawData(from url: String, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: completion)
        task.resume()
        return task
    }
    
    static func getDataDeserialized<T: Codable>(from url: String, completionHandler: @escaping (T?) -> Void) -> URLSessionDataTask {
        return getRawData(from: url) { data, response, error in
            
            guard let data = data, error == nil else {
                completionHandler(nil)
                return
            }
            
            guard let response = response as?  HTTPURLResponse else {
                completionHandler(nil)
                return
            }
            switch response.statusCode {
            case 200:
                do {
                    let deserializedData: T = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(deserializedData)
                    return
                } catch {
                    completionHandler(nil)
                    return
                }
            default:
                completionHandler(nil)
                return
            }
        }
    }
    
}
