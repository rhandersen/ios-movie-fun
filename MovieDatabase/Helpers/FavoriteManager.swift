import Foundation

struct FavoriteHelper {
    static func setFavorite(_ movie: Movie, isFavorite: Bool) {
        UserDefaults.standard.set(isFavorite, forKey: movie.title)
    }
    
    static func isFavorite(_ movie: Movie) -> Bool {
        return UserDefaults.standard.bool(forKey: movie.title)
    }
}
