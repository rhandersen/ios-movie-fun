struct Movie: Codable {
    let title: String
    let year: Int
    let director: String
    let stars: [String]
    let imageUrl: String
    let description: String
}
