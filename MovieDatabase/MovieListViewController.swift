import UIKit
import SnapKit

final class MovieListViewController: UIViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    var movieManager: MovieManager = MovieManager()
    var movies: [Movie] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        
        movieManager.fetchMovies { [weak self] movies in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.movies = movies ?? []
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedRow = tableView.indexPathForSelectedRow {
            self.tableView.reloadData()
            self.tableView.deselectRow(at: selectedRow, animated: animated)
        }
    }
}

extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.movies[indexPath.row]
        let movieDetailsViewController = MovieDetailsViewController(movie: movie)
        self.navigationController?.pushViewController(movieDetailsViewController, animated: true)
    }
}

extension MovieListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let movie = self.movies[indexPath.item]
        
        cell.textLabel?.text = "\(FavoriteHelper.isFavorite(movie) ? "⭐️ " : "💩")\(movie.title)"
        
        return cell
    }
}
