import UIKit

class ActorListViewController: UIViewController {
    
    var actors: [String] = []
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ActorTableViewCell.self, forCellReuseIdentifier: "ActorTableViewCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellType2")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 60
        return tableView
    }()
    
    init(movie: Movie) {
        super.init(nibName: nil, bundle: nil)
        self.actors = movie.stars
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ActorListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.actors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActorTableViewCell", for: indexPath)
        if let actorCell = cell as? ActorTableViewCell {
            actorCell.nameLabel.text = self.actors[indexPath.row]
            actorCell.emojiLabel.text = "🤓"
        }
        return cell
    }
}

extension ActorListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
}
