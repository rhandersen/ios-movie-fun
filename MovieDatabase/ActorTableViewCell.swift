import UIKit

class ActorTableViewCell: UITableViewCell {
    
    private(set) lazy var layoutStack: UIStackView = {
        let element = UIStackView(arrangedSubviews: [emojiLabel, nameLabel])
        element.axis = .horizontal
        return element
    }()
    
    private(set) lazy var emojiLabel: UILabel = {
        let uiLabel = UILabel()
        return uiLabel
    }()
    
    private(set) lazy var nameLabel: UILabel = {
        let uiLabel = UILabel()
        return uiLabel
    }()
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func commonInit() {
        self.addSubview(layoutStack)
        
        layoutStack.snp.makeConstraints({ make in
            make.edges.equalTo(self).inset(10)
        })
        
        emojiLabel.snp.makeConstraints({ make in
            make.width.equalTo(40)
            make.top.bottom.equalTo(layoutStack)
        })
        
        nameLabel.snp.makeConstraints({ make in
            make.top.bottom.equalTo(layoutStack)
        })
    }

}
