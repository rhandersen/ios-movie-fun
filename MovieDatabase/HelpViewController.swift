import UIKit

class HelpViewController: UIViewController, CustomViewDelegate {
    
    func didClick(_ sender: UIButton) {
        dismissDialog(sender)
    }
    
    private(set) lazy var closeButton: UIButton = {
        var button = UIButton()
        button.setTitle("CLOSE", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        return button
    }()
    
    private(set) lazy var customView: CustomView = {
        var customView = CustomView()
        customView.delegate = self
        customView.text1 = "Text 1 her"
        customView.text2 = "Text 2 bagefter i stakken"
        return customView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addElementsToView()
        styleView()
        populateWithData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func styleView() {
        view.backgroundColor = .white
        
        closeButton.snp.makeConstraints { make in
            make.centerX.centerY.equalTo(view.safeAreaLayoutGuide)
        }
        
        customView.snp.makeConstraints { make in
                make.centerX.equalTo(view.safeAreaLayoutGuide)
                make.top.equalTo(view.safeAreaLayoutGuide.snp.centerY).inset(10)
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(10)
                make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
    }
    
    private func addElementsToView() {
        view.addSubview(closeButton)
        view.addSubview(customView)
        self.closeButton.addTarget(self, action: #selector(self.dismissDialog(_:)), for: .touchUpInside)
    }
    
    private func populateWithData() {
        
    }
    
    @objc func dismissDialog(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
